﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance { get { return _instance; } }
    private static AudioManager _instance; 

    [SerializeField]
    private AudioSource audioSource;

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;//Avoid doing anything else
        }
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public void PlayBGM(string soundKey)
    {
        Debug.Log("Play Sound : " + soundKey);
        audioSource.Play();
    }
}
