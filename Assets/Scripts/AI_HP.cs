﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_HP : MonoBehaviour
{
    public float HP = 3;

    public SpriteRenderer sprite;
    public GameObject particlePref;
    private Rigidbody2D rb;
    [SerializeField] private float waitTime = 0;

    [SerializeField] private float flashRedTime = 0.2f;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }
    
    // Update is called once per frame
    void Update()
    {
        if (waitTime >= 0)
        {
            waitTime -= Time.deltaTime;
        }

        if (waitTime <= 0)
        {
            sprite.color = Color.white;
            waitTime = 0;
        }
    }

    public void GetHit(float damage)
    {
        HP -= damage;
        waitTime = flashRedTime;
        sprite.color = Color.red;
        //gameObject.GetComponent<AudioSource>().Play();
        if (HP <= 0)
        {
            if (GameManagerSingleton.Instance.HP <= GameManagerSingleton.Instance.MAX_HP - 1f)
            {
                GameManagerSingleton.Instance.HP++;
            }
            
            GameObject particles = Instantiate(particlePref, transform.position, transform.rotation);
            Destroy(particles,0.5f);
            Destroy(this.gameObject);
        }
    }
}
