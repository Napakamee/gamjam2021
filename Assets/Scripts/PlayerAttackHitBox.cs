﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackHitBox : MonoBehaviour
{
    public float knockForce = 20;
    public float damage;
    private Transform player;
    private float TimeToDisable;
    private AI_HP _aiHp;
    private void OnEnable()
    {
        TimeToDisable = Time.time + 0.2f;
    }

    private void Update()
    {
        damage = GameManagerSingleton.Instance.AttackDamage;
        if (Time.time > TimeToDisable)
        {
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            Rigidbody2D rb = other.GetComponent<Rigidbody2D>();
            rb.AddForce(-transform.right * knockForce,ForceMode2D.Impulse);
            _aiHp = other.GetComponent<AI_HP>();
            if (_aiHp != null)
            {
                _aiHp.GetHit(damage);
            }
            
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            //
        }
    }
}
