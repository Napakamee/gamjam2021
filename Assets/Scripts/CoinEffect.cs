﻿using System;
using System.Collections;
using System.Collections.Generic;
using ARTPATH.PLAYER;
using UnityEngine;

public class CoinEffect : MonoBehaviour
{
    public enum PowerUpEffect
    {
        AttackSpeed_to_point_seven,
        Damage_plus_one,
        Double_MaxHP,
        CDDecrease_to_point_two,
        AttackSpeed_to_point_four,
        Damage_plus_one2
    }

    public PowerUpEffect powerUpEffect = PowerUpEffect.AttackSpeed_to_point_seven;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //gameObject.GetComponent<AudioSource>().Play();
            PlayerController player = other.gameObject.GetComponent<PlayerController>();
            switch (powerUpEffect)
            {
                case PowerUpEffect.AttackSpeed_to_point_seven:
                    GameManagerSingleton.Instance.AttackSpeed = 0.2f;
                    GameManagerSingleton.Instance.nextscene = "SceneMoney";
                    GameManagerSingleton.Instance.pllight +=10f;
                    break;
                case PowerUpEffect.Damage_plus_one:
                    GameManagerSingleton.Instance.AttackDamage = 2;
                    GameManagerSingleton.Instance.nextscene = "Sceneslow";
                    GameManagerSingleton.Instance.pllight +=10f;
                    break;
                case PowerUpEffect.Double_MaxHP:
                    GameManagerSingleton.Instance.MAX_HP = 20;
                    GameManagerSingleton.Instance.nextscene = "SceneDark";
                    GameManagerSingleton.Instance.pllight +=10f;
                    break;
                case PowerUpEffect.CDDecrease_to_point_two:
                    GameManagerSingleton.Instance.CD = 0.2f;
                    GameManagerSingleton.Instance.nextscene = "SceneLight";
                    GameManagerSingleton.Instance.pllight +=10f;
                    break;
                case PowerUpEffect.AttackSpeed_to_point_four:
                    GameManagerSingleton.Instance.AttackSpeed = 0.1f;
                    GameManagerSingleton.Instance.nextscene = "SceneTheGank";
                    GameManagerSingleton.Instance.pllight +=10f;
                    break;
                case PowerUpEffect.Damage_plus_one2:
                    GameManagerSingleton.Instance.AttackDamage = 3;
                    GameManagerSingleton.Instance.bossFightReady = true;
                    GameManagerSingleton.Instance.pllight +=10f;
                    break;
            }

            player.IsPowerup = true;
            Destroy(gameObject);
        }
    }
}
