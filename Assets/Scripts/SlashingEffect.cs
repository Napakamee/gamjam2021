﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using ARTPATH.PLAYER;


public class SlashingEffect : MonoBehaviour
{
    public Animator _animator;
    public float _activateTime = 1.5f;
    public SpriteRenderer _SpriteRenderer;
    public float _activeTime = 1;
    public Collider2D _Collider2D;
    private Color[] colorIndex = { Color.white, Color.cyan, Color.green, Color.magenta, Color.red, Color.yellow};
    private Color color;

    private void Start()
    {
        color = colorIndex[Random.Range(0,colorIndex.Length)];
    }

    private void Update()
    {
        if (_activateTime > 0)
        {
            _activateTime-= Time.deltaTime;
            if(_activateTime <= 0)
            {_SpriteRenderer.color = Color.Lerp(_SpriteRenderer.color, color, .5f);}
        }
        else
        {
            _Collider2D.enabled = true;
            _animator.SetBool("IsSlash", true);
            if (_activeTime > 0)
            {
                _activeTime-= Time.deltaTime;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().ChangeHealth(-1);
        }
    }
}
