﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnOutScreen : MonoBehaviour
{
    
    [SerializeField] private float timeToDestroy = 3f;
    [SerializeField] private bool outbound = false;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("InBound"))
        {
            outbound = false;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("InBound"))
        {
            outbound = true;
        }
    }

    private void Update()
    {
        timeToDestroy -= Time.deltaTime;
        if (timeToDestroy <= 0)
        {
            Destroy(this.gameObject);
        }
        if (!outbound)
        {
            timeToDestroy = 3;
        }
    }
}
