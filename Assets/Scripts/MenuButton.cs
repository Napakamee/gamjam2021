﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButton : MonoBehaviour
{
	[SerializeField] MenuButtonController menuButtonController;
	[SerializeField] Animator animator;
	[SerializeField] AnimatorFunctions animatorFunctions;
	[SerializeField] int thisIndex;
	public Animator Panel;

    // Update is called once per frame
    void Update()
    {
		if(menuButtonController.index == thisIndex)
		{
			animator.SetBool ("selected", true);
			if(Input.GetAxis ("Submit") == 1){
				animator.SetBool ("pressed", true);
			}else if (animator.GetBool ("pressed")){
				animator.SetBool ("pressed", false);
				animatorFunctions.disableOnce = true;
			}
		}else{
			animator.SetBool ("selected", false);
		}

		if (menuButtonController.index == 0&& Input.GetAxis ("Submit") == 1)
		{
			StartCoroutine(OptionButton(SceneManager.GetActiveScene().buildIndex + 2));
			gameObject.GetComponent<AudioSource>().Stop();
		}

		if (menuButtonController.index == 1&& Input.GetAxis("Submit") == 1)
		{
			StartCoroutine(OptionButton(SceneManager.GetActiveScene().buildIndex + 1));
		}
		
    }

    IEnumerator OptionButton(int levelindex)
    {
	    Panel.SetTrigger("Tran");
	    yield return new WaitForSeconds(1f);
	    SceneManager.LoadScene(levelindex);
    }

    IEnumerator StartButton(int levelindex)
    {
	    Panel.SetTrigger("Tran");
	    yield return new WaitForSeconds(1f);
	    SceneManager.LoadScene(levelindex);
    }
}
