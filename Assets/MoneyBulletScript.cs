﻿using System;
using System.Collections;
using System.Collections.Generic;
using ARTPATH.PLAYER;
using UnityEngine;

public class MoneyBulletScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerController player = other.GetComponent<PlayerController>();
            player.ChangeHealth(-1);
            Destroy(this.gameObject);
        }
    }
}
