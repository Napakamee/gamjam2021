﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Platformer.Gameplay;
using static Platformer.Core.Simulation;
using Platformer.Model;
using Platformer.Core;
using UnityEngine.InputSystem;
using UnityEngine.PlayerLoop;

namespace Platformer.Mechanics
{
    public class PlayerController : KinematicObject
    {
        public AudioClip jumpAudio;
        public AudioClip respawnAudio;
        public AudioClip ouchAudio;

        /// Max horizontal speed of the player.
        public float maxSpeed = 7;
        /// Initial jump velocity at the start of a jump.
        public float jumpTakeOffSpeed = 7;

        public static PlayerController instance;
        
        private Rigidbody2D rb;
        public JumpState jumpState = JumpState.Grounded;
        private bool stopJump;
        public Collider2D collider2d;
         public AudioSource audioSource;
        public Health health;
        public bool controlEnabled = true;
        
        
        //Attack
        public Transform _AttackPoint;
        public float AttackRange = 0.5f;
        public LayerMask enemyLayer;
        
        public int noOfClicks = 0;
        float lastClickedTime = 0;
        public float maxComboDelay = 1.2f;
        
        //Dash
        public float ForceAttackInAir;
        public float DashSpeed;
        public float StartDashTime;
        private float DashTime;
        public bool dashing;
        private Vector3 MousePosition;
        private Vector2 Position = new Vector2(0f, 0f);
        public float dashDistance = 5.0f;
        public float dashDuration = 0.15f;

        bool jump;
        
        private Plane plane = new Plane(Vector3.up, Vector3.zero);
        Vector2 move;
        SpriteRenderer spriteRenderer;
        internal Animator animator;
        readonly PlatformerModel model = Simulation.GetModel<PlatformerModel>();
        private Camera cam;

        public Bounds Bounds => collider2d.bounds;

        void Awake()
        {
            instance = this;
            health = GetComponent<Health>();
            audioSource = GetComponent<AudioSource>();
            collider2d = GetComponent<Collider2D>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            animator = GetComponent<Animator>();
            rb = GetComponent<Rigidbody2D>();
            DashTime = StartDashTime;
            cam = Camera.main;
        }

        IEnumerator Forceinair()
        {
            Attack();
            Physics2D.gravity = new Vector2(0, 1);
            yield return new WaitForSeconds(0.4f);
            Physics2D.gravity = new Vector2(0, -9.18f);
        }
        IEnumerator DashRoutine (Vector3 direction)
        {
            if (this.dashDistance <= 0.001f)
                yield break;
 
            if (this.dashDuration <= 0.001f)
            {
                this.transform.position += direction * this.dashDistance;
                yield break;
            }
 
            this.dashing = true;
            var elapsed = 0f;
            var start = this.transform.position;
            var target = this.transform.position + this.dashDistance * direction;
 
            while (elapsed < this.dashDuration)
            {
                var iterTarget = Vector3.Lerp (start, target, elapsed / this.dashDuration);
                this.transform.position = iterTarget;
 
                yield return null;
                elapsed += Time.deltaTime;
            }
 
            this.transform.position = target;
            this.dashing = false;
        }
    
        protected override void Update()
        {
        if (!dashing)
         {
             if (Input.GetMouseButtonDown (1))
             {
                 var mousePos = cam.ScreenToWorldPoint (Input.mousePosition);
                 var direction = (mousePos - this.transform.position);
                 direction.z = 0;
 
                 if (direction.magnitude >= 0.1f)
                 {
                     this.StartCoroutine (this.DashRoutine (direction.normalized));
                 }
             }
         }
            Vector3 mousePosF = cam.ScreenToViewportPoint(Input.mousePosition);
            mousePosF = mousePosF.normalized;
            //Debug.Log("Mouse = " + mousePos);
            //Debug.Log("DashTime = " + DashTime);
            if (jumpState == JumpState.InFlight)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Debug.Log("AirAttack");
                    StartCoroutine(Forceinair());
                }
            }

            MousePosition = Input.mousePosition;
            MousePosition = Camera.main.ScreenToWorldPoint(MousePosition);
            
            #region Dash
            if (Input.GetMouseButtonDown(1))
            {
                Debug.Log("Force");
               rb.AddForce(mousePosF * 1000); 
            }
            if (mousePosF.x < 0.7f )
            {
                //spriteRenderer.flipX = true;
                transform.eulerAngles = new Vector3(0, 180, 0); 
            }
            if(mousePosF.x >= 0.7f )
            {
                //spriteRenderer.flipX = false;
                transform.eulerAngles = new Vector3(0, 0, 0); 
            }

            #endregion 
            if (controlEnabled)
            {
                move.x = Input.GetAxis("Horizontal");
                if (jumpState == JumpState.Grounded && Input.GetButtonDown("Jump"))
                    jumpState = JumpState.PrepareToJump;
                else if (Input.GetButtonUp("Jump"))
                {
                    stopJump = true;
                    Schedule<PlayerStopJump>().player = this;
                }
            }
            else
            {
                move.x = 0;
            }
            
            
            //combo
            if (Time.time - lastClickedTime > maxComboDelay)
            {
                noOfClicks = 0;
            }

            if (Input.GetMouseButtonDown(0))
            {
                noOfClicks++;
                if (noOfClicks == 1)
                {
                    animator.SetBool("Attack1", true);
                }
                noOfClicks = Mathf.Clamp(noOfClicks, 0, 3);
            }
            UpdateJumpState();
            base.Update();
        }
        
        
        
        public void Attack()
        {
            animator.SetTrigger("Attack");
            Collider2D[] AttackEnemy = Physics2D.OverlapCircleAll
                (_AttackPoint.position,AttackRange,enemyLayer);
            foreach (Collider2D enemy in AttackEnemy)
            {
                Debug.Log("Attack" + enemy.name);
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(_AttackPoint.position,AttackRange);
        }
        
        #region State
        void UpdateJumpState()
        {
            jump = false;
            switch (jumpState)
            {
                case JumpState.PrepareToJump:
                    jumpState = JumpState.Jumping;
                    jump = true;
                    stopJump = false;
                    break;
                case JumpState.Jumping:
                    if (!IsGrounded)
                    {
                        Schedule<PlayerJumped>().player = this;
                        jumpState = JumpState.InFlight;
                    }
                    break;
                case JumpState.InFlight:
                    if (IsGrounded)
                    {
                        Schedule<PlayerLanded>().player = this;
                        jumpState = JumpState.Landed;
                    }
                    break;
                case JumpState.Landed:
                    jumpState = JumpState.Grounded;
                    break;
            }
        }
        #endregion
        protected override void ComputeVelocity()
        {
            if (jump && IsGrounded)
            {
                velocity.y = jumpTakeOffSpeed * model.jumpModifier;
                jump = false;
            }
            else if (stopJump)
            {
                stopJump = false;
                if (velocity.y > 0)
                {
                    velocity.y = velocity.y * model.jumpDeceleration;
                }
            }

            /*if (move.x > 0.01f)
                spriteRenderer.flipX = false;
            else if (move.x < -0.01f)
                spriteRenderer.flipX = true;*/
            
            animator.SetBool("grounded", IsGrounded);
            animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

            targetVelocity = move * maxSpeed;
        }

        public enum JumpState
        {
            Grounded,
            PrepareToJump,
            Jumping,
            InFlight,
            Landed
        }
    }
}