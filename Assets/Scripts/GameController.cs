﻿using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Unity.Mathematics;
using Random = UnityEngine.Random;

namespace ARTPATH
{
    public class GameController : MonoBehaviour
    {

        [FoldoutGroup("SpawnerSetting", true, 0)] public bool IsSpawn;
        [FoldoutGroup("SpawnerSetting", true, 0)] public GameObject[] Spawners;
        [FoldoutGroup("SpawnerSetting", true, 0)] public LayerMask InMapMask;
        [FoldoutGroup("SpawnerSetting", true, 0)] public GameObject[] EnemiesList;
        [FoldoutGroup("SpawnerSetting", true, 0)] public float SpawnRate;
        
        [FoldoutGroup("SpawnerSetting", true, 0)] public GameObject Coin;
        [FoldoutGroup("SpawnerSetting", true, 0)] public GameObject[] CoinLocation;
        
        private float NextSpawn = 1;

        private void Start()
        {
            Spawners = GameObject.FindGameObjectsWithTag("Spawner");
            CoinLocation = GameObject.FindGameObjectsWithTag("CoinLocation");
            SpawnCoin();
        }

        private void Update()
        {
            if (NextSpawn > 0)
            {
                NextSpawn -= Time.deltaTime;
                if (NextSpawn <= 0)
                {
                    SpawnEnemy();
                    NextSpawn = SpawnRate;
                }
            }
        }

        private void SpawnEnemy()
        {
            bool[] isInMap = new bool[20];
            List<Transform> orderSpawn = new List<Transform>();
            for (int i = 0; i < isInMap.Length; i++)
            {
                isInMap[i] = Physics2D.OverlapCircle(Spawners[i].transform.position, 0.1f, InMapMask);
                if (isInMap[i])
                {
                    orderSpawn.Add(Spawners[i].transform);
                }
            }

            if (EnemiesList.Length > 1)
            {
                Instantiate(EnemiesList[Random.Range(0, EnemiesList.Length)],
                    orderSpawn[Random.Range(0, orderSpawn.Count)].position, Quaternion.identity);
            }
        }

        private void SpawnCoin()
        {
            Instantiate(Coin, CoinLocation[Random.Range(0, CoinLocation.Length)].transform.position,
                Quaternion.identity);
        }
    }
}
