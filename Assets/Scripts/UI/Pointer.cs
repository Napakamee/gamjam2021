﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pointer : MonoBehaviour
{
    [SerializeField] Animator animator;

    public Animator panel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerDown()
    {
        animator.SetBool ("selected", true);
        
    }
    public void OnPointerUp()
    {
        animator.SetBool ("selected", false);  
    }

    public void OnPointerEnter()
    {
        animator.SetBool ("pressed", true);
        StartCoroutine(OptionButton(SceneManager.GetActiveScene().buildIndex - 1));
    }
    public void OnPointerExit()
    {
        animator.SetBool ("pressed", false);
    }
    IEnumerator OptionButton(int levelindex)
    {
        panel.SetTrigger("Tran");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(levelindex);
    }
}
