﻿using System;
using Cinemachine;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace ARTPATH.PLAYER
{
    public class PlayerController : MonoBehaviour
    {
        public GameObject Player;
        public Rigidbody2D rb;
        public bool IsInBossFight;
        private Vector2 tragetVector2;
        public Animator animator;
        private SpriteRenderer _spriteRenderer;
        public Text YOULOSE;
        
        [FoldoutGroup("Player Setting", true, 0)] public int MAXHP;
        [FoldoutGroup("Player Setting", true, 0)] public float speed;
        [FoldoutGroup("Player Setting", true, 0)] public float limitedVelocity;
        [FoldoutGroup("Player Setting", true, 0)] public float JumpCD;
        [FoldoutGroup("Player Setting", true, 0)] public float jumpForce;
        [FoldoutGroup("Player Setting", true, 0)] public float dashForce;
        [FoldoutGroup("Player Setting", true, 0)] public Transform CenterDetector;
        [FoldoutGroup("Player Setting", true, 0)] public float CenterGroundDetectionRadius;
        [FoldoutGroup("Player Setting", true, 0)] public LayerMask groundMask;
        [FoldoutGroup("Player Setting", true, 0)] public LayerMask mouseAimMask;
        [FoldoutGroup("Player Setting", true, 0)] public float DashCD;
        [FoldoutGroup("Player Setting", true, 0)] public GameObject HitBoxHor;
        [FoldoutGroup("Player Setting", true, 0)] public GameObject HitBoxVer;

        [SerializeField] private int hp;
        [SerializeField] public Text hpTxt;
        private bool isGrounded;
        private bool isDashing;
        private float nextJump;
        private float nextDash;
        private float ChargeDurationRemain;
        private int CurrentAttackCombo;
        private float comboDelay;
        private float NextCombo;
        private float comboResetTime;
        private bool AllowJumpAfterDash;
        private Vector2 dir;
        private bool comboSwitch;
        private bool IsDoneAirAttack;
        private float AirAttackStop;
        private bool IsSpinning;
        private float invulnerable;
        public bool IsPowerup;
        private float PowerAnimTime;
        
        private void Start()
        {
            hp = GameManagerSingleton.Instance.MAX_HP;
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        void Update()
        {
            if (GameManagerSingleton.Instance.HP > 0)
            {
                if (IsPowerup)
                {
                    PowerUp();
                }

                if (Time.time > PowerAnimTime)
                {
                    animator.SetBool("IsPowerUp", false);
                }

                JumpCD = GameManagerSingleton.Instance.CD;
                DashCD = GameManagerSingleton.Instance.CD;
                comboDelay = GameManagerSingleton.Instance.AttackSpeed;
                hp = GameManagerSingleton.Instance.HP;
                hpTxt.text = "HP: " + hp;
                if (invulnerable > 0)
                {
                    invulnerable -= Time.deltaTime;
                    if (invulnerable <= 0)
                    {
                        _spriteRenderer.color = Color.white;
                    }
                }

                isGrounded = Physics2D.OverlapCircle(CenterDetector.position, CenterGroundDetectionRadius, groundMask);

                if (ChargeDurationRemain > 0)
                {
                    float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                    if (rb.velocity.x > 0 && CurrentAttackCombo == 0)
                    {
                        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward) * Quaternion.Euler(0, 180, 0);
                    }
                    else if (rb.velocity.x <= 0 && CurrentAttackCombo == 0)
                    {
                        transform.rotation = Quaternion.AngleAxis(180 + angle, Vector3.forward);
                    }

                    ChargeDurationRemain -= Time.deltaTime;
                }
                else
                {
                    if (isDashing)
                    {
                        this.transform.rotation = Quaternion.Euler(0, 180, 0);
                        isDashing = false;
                        rb.velocity /= 3;
                        rb.gravityScale = 2;
                        animator.SetBool("IsDashing", false);
                    }
                }

                if (AirAttackStop > 0)
                {
                    AirAttackStop -= Time.deltaTime;
                    if (AirAttackStop <= 0)
                    {
                        rb.velocity /= 3;
                    }
                }

                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, Mathf.Infinity, mouseAimMask))
                {
                    tragetVector2 = hit.point;
                    //Debug.Log(tragetVector2 + "hit");
                }

                if (Input.GetMouseButton(1) && nextDash >= DashCD)
                {
                    NextCombo = Time.time + comboDelay;
                    nextDash = 0;
                    rb.gravityScale = 0;
                    ChargeDurationRemain = .25f;
                    isDashing = true;
                    rb.isKinematic = false;
                    rb.velocity = (tragetVector2 - (Vector2) CenterDetector.position).normalized * dashForce;
                    dir = tragetVector2 - (Vector2) CenterDetector.position;
                    animator.SetBool("IsDashing", true);
                    HitBoxHor.SetActive(true);
                    invulnerable = 0.8f;
                    if (isGrounded)
                    {
                        AllowJumpAfterDash = true;
                    }
                }

                if (Input.GetMouseButton(0) && !IsDoneAirAttack)
                {
                    //ChargeDurationRemain = 0;
                    animator.SetBool("IsAttacking", true);
                    animator.SetBool("IsCombo", true);
                    if (!isGrounded)
                    {
                        rb.gravityScale = 0;
                    }

                    animator.SetBool("IsCombo", comboSwitch);

                    comboResetTime = Time.time + 0.7f;

                    if (CurrentAttackCombo == 0 && Time.time > NextCombo)
                    {
                        comboSwitch = !comboSwitch;
                        rb.velocity = Vector2.zero;
                        CurrentAttackCombo++;
                        NextCombo = Time.time + comboDelay;
                        animator.SetFloat("CurrentAttackCombo", CurrentAttackCombo);
                        HitBoxVer.SetActive(true);
                    }
                    else if (CurrentAttackCombo == 1 && Time.time > NextCombo)
                    {
                        comboSwitch = !comboSwitch;
                        rb.velocity = Vector2.zero;
                        CurrentAttackCombo++;
                        NextCombo = Time.time + comboDelay;
                        animator.SetFloat("CurrentAttackCombo", CurrentAttackCombo);
                        HitBoxVer.SetActive(true);
                    }
                    else if (CurrentAttackCombo == 2 && Time.time > NextCombo)
                    {
                        comboSwitch = !comboSwitch;
                        rb.velocity = Vector2.zero;
                        CurrentAttackCombo++;
                        NextCombo = Time.time + comboDelay * 2;
                        animator.SetFloat("CurrentAttackCombo", CurrentAttackCombo);
                        HitBoxHor.SetActive(true);
                        rb.velocity = -transform.right * dashForce / 2;
                        invulnerable = 0.8f;
                        IsDoneAirAttack = true;
                        AirAttackStop = 0.15f;
                    }
                    else if (CurrentAttackCombo == 3 && Time.time > NextCombo)
                    {
                        comboSwitch = !comboSwitch;
                        rb.velocity = Vector2.zero;
                        CurrentAttackCombo = 1;
                        NextCombo = Time.time + comboDelay;
                        animator.SetFloat("CurrentAttackCombo", CurrentAttackCombo);
                        HitBoxVer.SetActive(true);
                    }
                }

                if (Time.time > comboResetTime || Input.GetMouseButtonDown(1) || Input.GetKeyDown(KeyCode.Space))
                {

                    if (!isGrounded && CurrentAttackCombo != 0)
                    {
                        IsDoneAirAttack = true;
                    }

                    comboSwitch = !comboSwitch;
                    CurrentAttackCombo = 0;
                    rb.gravityScale = 2;
                    animator.SetFloat("CurrentAttackCombo", CurrentAttackCombo);
                    animator.SetBool("IsAttacking", false);

                }

                if (tragetVector2 != null && !isDashing)
                {
                    if (rb.velocity.x > 0 && CurrentAttackCombo == 0)
                    {
                        this.transform.rotation = Quaternion.Euler(0, 180, 0);
                    }
                    else if (rb.velocity.x < 0 && CurrentAttackCombo == 0)
                    {
                        this.transform.rotation = Quaternion.Euler(0, 0, 0);
                    }
                    else if (CurrentAttackCombo > 0)
                    {
                        if (tragetVector2.x - CenterDetector.position.x > 0)
                        {
                            this.transform.rotation = Quaternion.Euler(0, 180, 0);
                        }
                        else
                        {
                            this.transform.rotation = Quaternion.Euler(0, 0, 0);
                        }
                    }
                }

                if (Input.GetKeyDown(KeyCode.S))
                {
                    if (isGrounded)
                    {
                        IsSpinning = true;
                    }
                }

                if (isGrounded || AllowJumpAfterDash)
                {

                    if (isGrounded)
                    {
                        IsSpinning = false;
                        IsDoneAirAttack = false;
                        animator.SetBool("IsJumping", false);
                        animator.SetBool("IsClimbing", false);

                        if (nextJump < JumpCD)
                        {
                            nextJump += Time.deltaTime;
                        }

                        if (nextDash < DashCD)
                        {
                            nextDash = DashCD;
                        }
                    }

                    if (Input.GetKeyDown(KeyCode.Space) && nextJump >= JumpCD)
                    {
                        nextJump = 0;
                        rb.velocity = Vector2.up * jumpForce;
                        animator.SetBool("IsJumping", true);
                        AllowJumpAfterDash = false;
                    }
                }

                if (Input.GetAxis("Horizontal") != 0)
                {
                    animator.SetBool("IsRunning", true);
                    if (!isDashing)
                    {
                        rb.velocity = new Vector2(Input.GetAxis("Horizontal") * speed, rb.velocity.y);
                    }
                }
                else
                {
                    animator.SetBool("IsRunning", false);
                }

                animator.SetBool("IsGrounded", isGrounded);
            }
        }

        public void ChangeHealth(int value)
        {
            if (value < 0)
            {
                _spriteRenderer.color = Color.red;
                invulnerable = 0.5f;
            }

            GameManagerSingleton.Instance.HP += value;
            if (GameManagerSingleton.Instance.HP <= 0)
            {
                animator.SetBool("IsDead", true);
                rb.isKinematic = true;
                rb.velocity = Vector2.zero;
                YOULOSE.gameObject.SetActive(true);
                _spriteRenderer.color = Color.white;
                Invoke("ReloadScene", 5);
            }
        }
    
        

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Enemy"))
            {
                if (IsSpinning)
                {
                    //other.gameObject.GetComponent<>()
                }
                else
                {
                    if (invulnerable <= 0)
                    {
                        ChangeHealth(-1);
                    }
                }
            }
        }

        private void PowerUp()
        {
            IsPowerup = false;
            animator.SetBool("IsPowerUp", true);
            PowerAnimTime = Time.time + 1.5f;
            invulnerable = 3f;
        }

        private void ReloadScene()
        {
            rb.isKinematic = false;
            GameManagerSingleton.Instance.HP = GameManagerSingleton.Instance.MAX_HP;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
