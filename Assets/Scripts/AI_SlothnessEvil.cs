﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace ARTPATH.AI
{
    public class AI_SlothnessEvil : MonoBehaviour
    {
        [Title("Set-up")] 
        public Animator animator;

        private Rigidbody2D rb;

        private GameObject player;

        [FoldoutGroup("AI Setting", true, 0)] public int HP = 3;
        [FoldoutGroup("AI Setting", true, 0)] public float speed;
        [FoldoutGroup("AI Setting", true, 0)] public Transform ForwardDetector;
        [FoldoutGroup("AI Setting", true, 0)] public float ForwardGroundDetectionDistance;
        [FoldoutGroup("AI Setting", true, 0)] public float ForwardObstacleDetectionDistance;
        [FoldoutGroup("AI Setting", true, 0)] public LayerMask groundMask;
        [FoldoutGroup("AI Setting", true, 0)] public float limitedVelocity;
        private float MobilityPercentage = 100;
        private bool isMovingRight = true;

        private void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
            player = GameObject.Find("PlayerLocation");
        }

        private void Update()
        {
            Patrolling();

            Debug.DrawRay(ForwardDetector.position, Vector2.down * ForwardGroundDetectionDistance,
                Color.green);
            Debug.DrawRay(ForwardDetector.position, transform.right * ForwardObstacleDetectionDistance, 
                Color.green);

            if (Mathf.Abs(rb.velocity.x) > limitedVelocity)
            {
                rb.AddForce(new Vector2(-rb.velocity.x, 0));
            }
        }

        private void Patrolling()
        {

            RaycastHit2D groundInfo = Physics2D.Raycast(ForwardDetector.position, Vector2.down,
                ForwardGroundDetectionDistance, groundMask);
            RaycastHit2D obstacleInfo = Physics2D.Raycast(ForwardDetector.position, Vector2.right,
                ForwardObstacleDetectionDistance, groundMask);

            rb.velocity = new Vector2(transform.right.x * speed *
                          Time.deltaTime * MobilityPercentage,rb.velocity.y);

            if (!groundInfo.collider || obstacleInfo)
            {
                switch (isMovingRight)
                {
                    case true:
                        transform.eulerAngles = new Vector3(0, 0, 0);
                        isMovingRight = false;
                        break;
                    case false:
                        transform.eulerAngles = new Vector3(0, 180, 0);
                        isMovingRight = true;
                        break;
                }
            }
        }
    }
}

