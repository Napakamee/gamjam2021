﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace ARTPATH.AI
{
    public class AI_DeadEndEvil : MonoBehaviour
    {
        [Title("Set-up")] 
        public Animator animator;
        
        private Rigidbody2D rb;

        private GameObject player;

        [FoldoutGroup("AI Setting", true, 0)] public int HP = 3;
        [FoldoutGroup("AI Setting", true, 0)] public float speed;
        [FoldoutGroup("AI Setting", true, 0)] public float limitedVelocity;

        private Color DebugLineOfSight;

        private void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
            player = GameObject.Find("PlayerLocation");
        }

        private void Update()
        {
            if (player != null)
            {
                if (player.transform.position.x - transform.position.x > 0)
                {
                    this.transform.rotation = Quaternion.Euler(0, 180, 0);
                }
                else
                {
                    this.transform.rotation = Quaternion.Euler(0, 0, 0);
                }

                FloatingChasingMechanic();
                if (rb.velocity.magnitude > limitedVelocity)
                {
                    rb.AddForce(-rb.velocity);
                }
            }
        }

        private void FloatingChasingMechanic()
        {
            Vector2 dir = player.transform.position - this.transform.position;
            rb.AddForce(dir.normalized*speed);
        }
    }
}