﻿using System;
using ARTPATH.PLAYER;
using UnityEngine;

public class GPen_Projectiles : MonoBehaviour
{
    public float timeBeforeRelease;
    public Transform pivot;
    public Rigidbody2D rb;
    private int ricochetRemain = 5;
    public float penforce = 10;
    public LayerMask ricochetMask;
    private Vector2 currentDir;
    private bool IsInMap;
    public Transform OutBoundDetector;
    public LayerMask InMapCheckerMask;
    private bool IsLaunched;
    
    void FixedUpdate()
    {
        IsInMap = Physics2D.OverlapCircle(OutBoundDetector.position, 1, InMapCheckerMask);
        
        if (timeBeforeRelease > 0)
        {
            transform.RotateAround(pivot.position, Vector3.forward, 180 * Time.deltaTime);
            timeBeforeRelease -= Time.deltaTime;
        }
        else if (timeBeforeRelease <= 0&& !IsLaunched)
        {
            transform.RotateAround(pivot.position, Vector3.forward, 180 * Time.deltaTime);
            timeBeforeRelease -= Time.deltaTime;
            if (IsInMap )
            {
                IsLaunched = true;
                rb.angularVelocity = 0;
                rb.velocity = transform.up * penforce;
            }
        }
        else
        {
            Ray2D ray = new Ray2D(transform.position, transform.up);
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 1.0f, ricochetMask);

                if (hit.collider != null && hit.transform.tag == "Ground")
                {
                    Vector2 v = Vector2.Reflect(ray.direction, hit.normal);
                    float rot = 90 - Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
                    transform.eulerAngles = new Vector3(0, 0, -rot);
                    rb.velocity = transform.up * penforce;
                    ricochetRemain--;
                }

                if (ricochetRemain <= 0)
                {
                    Destroy(this.gameObject);
                }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().ChangeHealth(-1);
        }
    }
}
