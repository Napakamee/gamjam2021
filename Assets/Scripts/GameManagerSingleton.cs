﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManagerSingleton : Singleton<GameManagerSingleton>
{
    protected GameManagerSingleton(){}

    public int MAX_HP = 10;
    public int HP = 10;
    public int AttackDamage = 1;
    public float AttackSpeed= 0.3f;
    public float CD = 0.2f;
    public string nextscene = "SceneDraw";
    public bool bossFightReady = false;
    public float pllight = 10;
}
