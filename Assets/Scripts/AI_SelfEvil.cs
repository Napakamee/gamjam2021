﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;
using Random = UnityEngine.Random;

namespace ARTPATH.AI
{
    public class AI_SelfEvil : MonoBehaviour
    {
        public enum BossState
        {
            idle,
            intro,
            chasing,
            attack,
            swordSummon,
            swordReturn,
            blinkBehide,
            chasing2,
            attack2,
            screenSlash
        }
        [Title("Set-up")] 
        public Animator animator;
        
        private Rigidbody2D rb;

        private GameObject player;
        public BossState _BossState = BossState.idle;

        public BossFall BossFall;
        
        [FoldoutGroup("AI Setting", true, 0)] public float animationLength = 1;
        [FoldoutGroup("AI Setting", true, 0)] public float speed;
        [FoldoutGroup("AI Setting", true, 0)] public float jumpForce;
        [FoldoutGroup("AI Setting", true, 0)] public float height;
        [FoldoutGroup("AI Setting", true, 0)] public Transform ForwardDetector;
        [FoldoutGroup("AI Setting", true, 0)] public float ForwardGroundDetectionDistance;
        [FoldoutGroup("AI Setting", true, 0)] public float ForwardObstacleDetectionDistance;
        [FoldoutGroup("AI Setting", true, 0)] public float ForwardRoofDetectionDistance;
        [FoldoutGroup("AI Setting", true, 0)] public Transform CenterDetector;
        [FoldoutGroup("AI Setting", true, 0)] public float CenterGroundDetectionRadius;
        [FoldoutGroup("AI Setting", true, 0)] public float CenterGroundDetectionDistance;
        [FoldoutGroup("AI Setting", true, 0)] public float CenterRoofDetectionDistance;
        [FoldoutGroup("AI Setting", true, 0)] public float JumpCD;
        [FoldoutGroup("AI Setting", true, 0)] public LayerMask groundMask;
        [FoldoutGroup("AI Setting", true, 0)] public LayerMask LineOfSightMask;
        [FoldoutGroup("AI Setting", true, 0)] public float DetectingDistance;
        [FoldoutGroup("AI Setting", true, 0)] public float screenSlashDelay;
        [FoldoutGroup("AI Setting", true, 0)] public GameObject GPen_Projectiles;
        [FoldoutGroup("AI Setting", true, 0)] public GameObject GPenPivot;
        [FoldoutGroup("AI Setting", true, 0)] public Transform PenSpawnPoint;
        [FoldoutGroup("AI Setting", true, 0)] public Transform[] SummonSwordLocation;
        [FoldoutGroup("AI Setting", true, 0)] public GameObject Slashing;
        [FoldoutGroup("Player Setting", true, 0)] public GameObject HitBoxHor;
        [FoldoutGroup("Player Setting", true, 0)] public GameObject HitBoxVer;
        
        private float swordCount = 12;private float swordCount2 = 50;
        
        private float MobilityPercentage = 100;
        private float nextJump = 1;
        private bool isGrounded = false;
        private float nextAction = 2;
        private bool IsMachanicEnded = false;
        private int currentSwordCount = 0;
        private int random;
        
        private int currentAttackCombo = 0;
        private bool fallingAttackState = false;

        private SpriteRenderer _spriteRenderer;
        private bool IsFacingLocked = false;

        private float AnimAttackSpeed = 2;
        
        private void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
            player = GameObject.Find("PlayerLocation");
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }
        
        void Update()
        {
            if (!IsFacingLocked)
            {
                if (player.transform.position.x - transform.position.x > 0)
                {
                    this.transform.rotation = Quaternion.Euler(0, 180, 0);
                }
                else
                {
                    this.transform.rotation = Quaternion.Euler(0, 0, 0);
                }
            }

            isGrounded = Physics2D.OverlapCircle(CenterDetector.position, CenterGroundDetectionRadius, groundMask);
            
            switch (_BossState)
            {
                case BossState.idle:
                    Idle();
                    break;
                case BossState.intro:
                    Intro();
                    break;
                case BossState.chasing:
                    Chasing();
                    break;
                case BossState.attack:
                    Attack();
                    break;
                case BossState.swordSummon:
                    SwordSummon();
                    break;
                case BossState.swordReturn:
                    SwordReturn();
                    break;
                case BossState.blinkBehide:
                    BlinkBehide();
                    break;
                case BossState.chasing2:
                    Chasing();
                    break;
                case BossState.attack2:
                    Attack();
                    break;
                case BossState.screenSlash:
                    ScreenSlash();
                    break;
                default: Chasing(); break;
            }
        }

        void Idle()
        {
            if (Input.GetKey(KeyCode.P))
            {
                _BossState = BossState.intro;
            }
        }

        void Intro()
        {
            animationLength -= Time.deltaTime;
            if (animationLength < 0)
            {
                _BossState = BossState.chasing;
                nextAction = Time.time + 10;
            }

            gameObject.AddComponent<BossFall>();
        }

        void Chasing()
        {
            IsFacingLocked = false;
            DefaultChasingMechanic();
            if (Time.time > nextAction)
            {
                if (_BossState == BossState.chasing)
                {
                    _BossState = BossState.attack;
                    nextAction = Time.time + 0.15f;
                    rb.velocity = new Vector2(rb.velocity.x/2, rb.position.y);
                }
                else if (_BossState == BossState.chasing2)
                {
                    _BossState = BossState.attack2;
                    nextAction = Time.time + 0.15f;
                    rb.velocity = new Vector2(rb.velocity.x/2, rb.position.y);
                }
                animator.SetBool("IsChasing", false);
                animator.SetBool("IsFallingAttack", false);
            }
        }

        void Attack()
        {
            IsFacingLocked = true;
            if (Time.time > nextAction)
            {
                if (isGrounded)
                {
                    DefaultAttackMechianic();
                }

                if (IsMachanicEnded)
                {
                    if (_BossState == BossState.attack)
                    {
                        _BossState = BossState.swordSummon;
                        GPenPivot.transform.eulerAngles = new Vector3(0, 0, Random.Range(0, 360));
                        random = Random.Range(0, 3);
                        IsMachanicEnded = false;
                        animator.SetBool("IsFloating", true);
                        IsFacingLocked = true;
                        rb.AddForce(new Vector2((SummonSwordLocation[random].position.x - transform.position.x), jumpForce), ForceMode2D.Impulse);
                        nextAction = Time.time + 0.5f;
                    }
                    else if (_BossState == BossState.attack2)
                    {
                        _BossState = BossState.screenSlash;
                        nextAction = Time.time + screenSlashDelay;
                        this.GetComponent<Collider2D>().enabled = true;
                        IsMachanicEnded = false;
                        IsFacingLocked = true;
                        animator.SetBool("IsShadowFade", true);
                        rb.isKinematic = true;
                        rb.velocity = Vector2.zero;
                    }
                }
            }
        }

        void SwordSummon()
        {
            if (Time.time > nextAction)
            {
                rb.gravityScale = 0;
                rb.velocity = Vector2.zero;
                rb.isKinematic = true;
                transform.position = Vector2.MoveTowards(transform.position, SummonSwordLocation[random].position,
                    Time.deltaTime * speed * 3);
                if (transform.position == SummonSwordLocation[random].position && currentSwordCount < swordCount &&
                    Time.time > nextAction)
                {
                    GameObject gpen = Instantiate(GPen_Projectiles, PenSpawnPoint.position, PenSpawnPoint.rotation);
                    GPen_Projectiles gPenProjectiles = gpen.GetComponent<GPen_Projectiles>();
                    gPenProjectiles.timeBeforeRelease = 3 + currentSwordCount * 0.35f;
                    gPenProjectiles.pivot = GPenPivot.transform;

                    currentSwordCount++;
                    nextAction = Time.time + 0.15f;
                }
                else if (currentSwordCount >= swordCount)
                {
                    nextAction = Time.time + 8f;
                    _BossState = BossState.swordReturn;
                    rb.velocity = Vector2.zero;
                    currentSwordCount = 0;
                    IsFacingLocked = false;
                }
            }
        }

        void SwordReturn()
        {
            if (Time.time > nextAction)
            {
                rb.isKinematic = false;
                rb.gravityScale = 2;
                Vector2 dir = player.transform.position - this.transform.position;

                    rb.AddForce(Vector2.down * 3);
                    fallingAttackState = true;
                    animator.SetBool("IsFloating", false);
                    animator.SetBool("IsFallingAttack", true);

                    if (fallingAttackState)
                {
                    RaycastHit2D GroundInfo = Physics2D.Raycast(transform.position,
                        Vector2.down,
                        CenterGroundDetectionDistance, groundMask);
                    if (GroundInfo)
                    {
                        fallingAttackState = false;
                        animator.SetBool("IsFallingAttack", false);
                    }
                }
                if (isGrounded)
                {
                    //Instantiate(FIREGROUND)
                    _BossState = BossState.blinkBehide;
                    nextAction = Time.time + 1.2f;
                    animator.SetBool("IsFallingAttack", false);
                    animator.SetBool("IsShadowFade", true);
                }
            }
        }

        void BlinkBehide()
        {
            if (Time.time > nextAction)
            {
                if (currentAttackCombo == 0)
                {
                    this.GetComponent<Collider2D>().enabled = false;
                    rb.isKinematic = true;
                    nextAction = Time.time + 1.5f;
                }
                if (currentAttackCombo == 0)
                {
                    RaycastHit2D checkPlayerInfo = Physics2D.Raycast(player.transform.position, player.transform.right,
                        2, groundMask);
                    if (checkPlayerInfo.collider)
                    {
                        transform.position =  checkPlayerInfo.point - (Vector2)checkPlayerInfo.transform.right* 0.5f + Vector2.up*0.5f;
                    }
                    else
                    {
                        transform.position = (Vector2)player.transform.position + (Vector2)player.transform.right * 1.5f + Vector2.up *0.5f;
                    }
                    currentAttackCombo++;
                }

                else if (currentAttackCombo == 1)
                {
                    this.GetComponent<Collider2D>().enabled = true;
                    rb.isKinematic = false;
                    if (isGrounded)
                    {
                        currentAttackCombo++;
                        nextAction = Time.time + 1.25f;
                        animator.SetBool("IsShadowFade", false);
                    }
                }

                else if (currentAttackCombo == 2)
                {
                    IsFacingLocked = true;
                    currentAttackCombo++;
                    nextAction = Time.time + 0.5f;
                }
                else if (currentAttackCombo == 3)
                {
                    HitBoxHor.SetActive(true);
                    animator.SetBool("IsDash", true);
                    currentAttackCombo++;
                    rb.velocity= -transform.right * 30;
                    nextAction = Time.time + 0.3f;
                }
                else
                {
                    currentAttackCombo = 0;
                    rb.velocity = Vector2.zero;
                    animator.SetBool("IsDash", false);
                    _BossState = BossState.chasing2;
                    nextAction = Time.time + 12f;
                }
            }
            Debug.DrawRay(player.transform.position, player.transform.right * 2,
                Color.green);
        }

        void ScreenSlash()
        {
            if (isGrounded)
            {
                if (currentSwordCount < swordCount2 && Time.time > nextAction)
                {
                    nextAction = Time.time + 0.20f;
                    currentSwordCount++;
                    GameObject penInk = Instantiate(Slashing, new Vector2(Random.Range(-11, 11), Random.Range(-5, 7)),
                        Quaternion.Euler(0, 0, Random.Range(0, 360)));
                }
                else if (currentSwordCount >= swordCount2 && Time.time > nextAction && currentAttackCombo == 0)
                {
                    animator.SetBool("IsShadowFade", false);
                    nextAction = Time.time + 1.2f;
                    currentAttackCombo++;
                }
                else if (Time.time > nextAction && currentAttackCombo == 1)
                {
                    this.GetComponent<Collider2D>().enabled = true;
                    currentSwordCount = 0; Debug.Log(currentSwordCount);
                    rb.isKinematic = false;
                    _BossState = BossState.chasing;
                    nextAction = Time.time + 10f;
                    currentAttackCombo = 0;
                }
            }
        }
        
        private void DefaultChasingMechanic()
        {
            Vector2 dir = player.transform.position - this.transform.position;
            if (rb.velocity == Vector2.zero)
            {
                animator.SetBool("IsChasing", false);
            }
            else
            {
                animator.SetBool("IsChasing", true);
            }

            if (player != null)
            {
                RaycastHit2D groundInfo = Physics2D.Raycast(ForwardDetector.position, Vector2.down,
                    ForwardGroundDetectionDistance, groundMask);
                RaycastHit2D obstacleInfo = Physics2D.Raycast(ForwardDetector.position, Vector2.right,
                    ForwardObstacleDetectionDistance, groundMask);
                RaycastHit2D roofInfo = Physics2D.Raycast(ForwardDetector.position, Vector2.up,
                    ForwardRoofDetectionDistance, groundMask);
                RaycastHit2D aboveHeadInfo = Physics2D.Raycast(CenterDetector.position + Vector3.up, Vector2.up,
                    CenterRoofDetectionDistance, groundMask);

                rb.AddForce(new Vector2((player.transform.position.x - transform.position.x), 0).normalized * speed *
                            Time.deltaTime * MobilityPercentage);

                if ((groundInfo.collider && !obstacleInfo.collider &&
                     Mathf.Abs(transform.position.x - player.transform.position.x) >
                     transform.position.y - player.transform.position.y - height &&
                     player.transform.position.y - transform.position.y < height))
                {
                    MobilityPercentage = 100;
                }
                else if (Time.time > nextJump && isGrounded &&
                         player.transform.position.y - transform.position.y > height)
                {
                    if (!roofInfo.collider && Mathf.Abs(transform.position.x - player.transform.position.x) <
                        transform.position.y - player.transform.position.y - height)
                    {
                        MobilityPercentage = 70;
                        nextJump = Time.time + JumpCD;
                        rb.velocity = Vector2.up * jumpForce;
                    }
                    else if (!aboveHeadInfo.collider)
                    {
                        MobilityPercentage = 70;
                        nextJump = Time.time + JumpCD;
                        rb.velocity = Vector2.up * jumpForce;
                    }
                }
                else
                {
                    MobilityPercentage = 100;
                }

                if (!isGrounded && player.transform.position.y < transform.position.y && Mathf.Abs(dir.x) <= 3f &&
                    Mathf.Abs(dir.y)>= 2)
                {
                    RaycastHit2D PlayerInfo = Physics2D.Raycast(transform.position,
                        dir,
                        Mathf.Infinity, LineOfSightMask);
                    if (PlayerInfo.distance <= DetectingDistance && PlayerInfo.collider.CompareTag("Player"))
                    {
                        rb.AddForce(Vector2.down * 3);
                        fallingAttackState = true;
                        animator.SetBool("IsFallingAttack", true);
                    }
                }

                if (fallingAttackState)
                {
                    RaycastHit2D GroundInfo = Physics2D.Raycast(transform.position,
                        Vector2.down,
                        CenterGroundDetectionDistance, groundMask);
                    if (GroundInfo)
                    {
                        fallingAttackState = false;
                        animator.SetBool("IsFallingAttack", false);
                    }
                }
            }
        }

        private void DefaultAttackMechianic()
        {
            AnimAttackSpeed = 1.5f; 
            animator.SetFloat("AttackSpeed", AnimAttackSpeed);
            if (currentAttackCombo == 0 && Time.time > nextAction)
            {
                currentAttackCombo++;
                animator.SetInteger("AttackingState", 1);
                HitBoxVer.SetActive(true);
            }

            else if (currentAttackCombo == 1 && Time.time > nextAction)
            {
                currentAttackCombo++;
                nextAction = Time.time + 0.2f; //combo2
                animator.SetInteger("AttackingState", 2);
                HitBoxVer.SetActive(true);
            }

            else if (currentAttackCombo == 2 && Time.time > nextAction)
            {
                currentAttackCombo++;
                nextAction = Time.time + 0.2f; //combo3
                animator.SetInteger("AttackingState", 3);
                HitBoxHor.SetActive(true);
            }

            else if (currentAttackCombo == 3 && Time.time > nextAction)
            {
                currentAttackCombo++;
                nextAction = Time.time + 0.2f;
                rb.velocity = -transform.right * 30;
            }
            else if (currentAttackCombo == 4 && Time.time > nextAction)
            {
                currentAttackCombo++;
                nextAction = Time.time + 0.1f;
                rb.velocity = Vector2.zero;
            }
            else if (Time.time > nextAction)
            {
                currentAttackCombo = 0;
                IsMachanicEnded = true;
                animator.SetInteger("AttackingState", 0);
            }
        }

    }
}
