﻿using System.Collections;
using System.Collections.Generic;
using ARTPATH.AI;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class BossSceneController : MonoBehaviour
{
    public GameObject Door;
    public GameObject Door2;
    public Light2D playerlight;

    void Start()
    {
        Door.GetComponent<DoorScript>().nextScene = GameManagerSingleton.Instance.nextscene;
        playerlight.pointLightOuterRadius = GameManagerSingleton.Instance.pllight;
        if (GameManagerSingleton.Instance.bossFightReady)
        {
            Door.SetActive(false);
            Door2.SetActive(false);
        }
    }
    
}
