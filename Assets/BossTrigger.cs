﻿using System;
using System.Collections;
using System.Collections.Generic;
using ARTPATH.AI;
using UnityEngine;

public class BossTrigger : MonoBehaviour
{
    public AI_SelfEvil boss;
    public GameObject door;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            boss._BossState = AI_SelfEvil.BossState.intro;
            door.SetActive(true);
            Destroy(this.gameObject);
        }
    }
}

