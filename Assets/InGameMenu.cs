﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameMenu : MonoBehaviour
{
    public Animator panel;
    public void OnPointerEnter()
    {
        StartCoroutine(OptionButton(SceneManager.GetActiveScene().buildIndex - 2));
    }
    IEnumerator OptionButton(int levelindex)
    {
        panel.SetTrigger("Tran");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(levelindex);
    }
}
